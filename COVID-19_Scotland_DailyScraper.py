import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
#import seaborn as sns
get_ipython().run_line_magic('matplotlib', 'inline')
from datetime import date
import datetime
from datetime import timedelta  
from urllib.request import urlopen
from bs4 import BeautifulSoup
import re
import yagmail
import dropbox


#Determine Env Variables
tDt = date.today()
yDt = tDt - timedelta(days=1)

#Make Some Soup 

url = "https://www.gov.scot/coronavirus-covid-19/"
html = urlopen(url)
soup = BeautifulSoup(html, 'lxml')

text = soup.get_text()
rows = soup.find_all('tr')
for row in rows:
    row_td = row.find_all('td')
str_cells = str(row_td)
cleantext = BeautifulSoup(str_cells, "lxml").get_text()

list_rows = []
for row in rows:
    cells = row.find_all('td')
    str_cells = str(cells)
    clean = re.compile('<.*?>')
    clean2 = (re.sub(clean, '',str_cells))
    list_rows.append(clean2)

df = pd.DataFrame(list_rows)
df1 = df[0].str.split(',', expand=True)
df1[0] = df1[0].str.strip('[')
df1.head(10)
col_labels = soup.find_all('th')
all_header = []
col_str = str(col_labels)
cleantext2 = BeautifulSoup(col_str, "lxml").get_text()
all_header.append(cleantext2)
df2 = pd.DataFrame(all_header)
df3 = df2[0].str.split(',', expand=True)
frames = [df3, df1]
df4 = pd.concat(frames)
df5 = df4.rename(columns=df4.iloc[0])
df6 = df5.dropna(axis=0, how='any')
df7 = df6.drop(df6.index[0])
df7.rename(columns={'[Health board': 'Health_Board'},inplace=True)
df7.rename(columns={' Positive cases]': 'Positive_Cases'},inplace=True)
df7['Positive_Cases'] = df7['Positive_Cases'].str.strip(']')
df7['Positive_Cases'] = df7['Positive_Cases'].astype(int)
df7.iloc[0,0] = 'Ayrshire'

#Import Yesterday's Case Data
df_yDt = pd.read_excel(r'filepath\\SARS-Cov-2-Scotland-{}_raw.xlsx'.format(yDt), sheet_name='Total Cases')
#df_yDt.iloc[0,1] = 'Ayrshire'
#Drop last two and first column
cols = [0,-1,-2]
df_yDt = df_yDt.drop(df_yDt.columns[[cols],], axis=1) 
#Change date format
mapper = lambda x: x.strftime("%Y-%m-%d") if isinstance(x, datetime.datetime) else x
df_yDt.columns = df_yDt.columns.map(mapper)
#Merge today's data with yesterday's
df_x = pd.merge(df_yDt, df7, how = 'right')
df_x = df_x.rename(columns={"Positive_Cases":(tDt)})
#Deal with nulls
df_x2=df_x.fillna(0)
#Deal with DateTime formatting
df_x.columns = df_x.columns.map(mapper)
#Total sum per column (except Health_Board)
df_x2.loc['Total']= df_x.iloc[:, 1:].sum(axis=0)
df_x2 = df_x2.replace(np.nan, 'Total', regex=True)
#Deal with DateTime formatting
df_x2.columns = df_x2.columns.map(mapper)
#Plot the epiCurve
df_plot = df_x2.head(n=0)
df_plot = df_x2.tail(n=1) 
#Drop Health_Board Column
df_plot2 = df_plot.drop(['Health_Board'], axis=1)
df_plot2 = df_plot2.transpose()
#This Commented out code below plots cases only (no deaths)
#plot = df_plot2.plot.line(title = 'COVID in Scotland', rot=45)
#plot.set_ylabel("cumNumCases")
#fig1 = plot.get_figure()
#fig1.savefig(r'filepath\\cumNumScotland_cases_Plot_{}.jpg'.format(tDt),bbox_inches="tight")


#Import Yesterday's Death Data
df_Deaths = pd.read_excel (r'filepath\\SARS-Cov-2-Scotland-{}_deaths_raw.xlsx'.format(yDt), sheet_name='Deaths') 
#Drop first column
df_Deaths = df_Deaths.drop(df_Deaths.columns[[0],], axis=1) 
#Report deaths
result_3 = re.search('(.*) patients who tested positive for coronavirus (.*) have died', text)
total_deaths = (result_3.group(1))
tot_deaths = int(total_deaths)
#Deaths Calculations
#tot_deaths
old_deaths = df_Deaths.iloc[1,-1]
new_deaths = tot_deaths-old_deaths
#new_deaths
dict_1 = {new_deaths:'Deaths_New', tot_deaths: 'Deaths_Cum' }
df_Deaths[tDt] = dict_1
#Transpose
df_Deaths_Tran = df_Deaths.transpose()
#Calculate % increase of deaths
m = round((new_deaths/old_deaths)*100,1)
txt_5 = "The death toll is currently {}. The number of new deaths today is {}, which represents a {}% increase on yesterday's total.".format(total_deaths, new_deaths, m)
#Sort out formatting for deaths for future imports
df_DeathsX = df_Deaths
#df_DeathsX[tDt] = [new_deaths, tot_deaths]
#Transpose
df_Deaths_XTran = df_Deaths.transpose()
#Plot cases and deaths together
df_Deaths2 = df_Deaths.drop(0)
df_Deaths3 = df_Deaths2.transpose()
#df_plot2
df_Deaths3 = df_Deaths2.transpose()
df_Deaths4 = df_Deaths3.drop(['Date'])
#Format datetimes
df_Deaths.columns = df_Deaths.columns.map(mapper)
df_Deaths4.columns = ['Deaths']
#Merge Death Data and Case Data
df_casesDeaths = pd.merge(df_plot2, df_Deaths4, how = 'left',  left_index=True, right_index =True)
df_casesDeaths = df_casesDeaths.rename(columns={"Total":('Cases')})
#Plot the epiCurve and deathCurve
plot = df_casesDeaths.plot.line(title = 'COVID in Scotland', rot=45)
plot.set_ylabel("cum Num")
fig = plot.get_figure()
fig.savefig(r'filepath\\cumNumScotland_casesDeaths_Plot_{}.jpg'.format(tDt),bbox_inches="tight")
#fig.show()

#Find number of new cases today, by Health_Board
df_Old = df_x2.iloc[:, [0,-2]] 
df_New = df_x2.iloc[:, [0,-1]] 
df_Join = pd.merge(df_Old, df_New, how = 'right', on = ['Health_Board'])
df_Join['newCases'] = (df_Join.iloc[:,-1]-df_Join.iloc[:,-2])
#What is the total increase in case numbers across the whole country?
x = df_x2.iloc[-1,-2]
y = df_x2.iloc[-1,-1]
y = int(y)
z = int(y-x)
p = (z/x)*100
p2 = round(p,1)
txt_1 = "The number of new cases today is {}, which represents a {}% increase on yesterday's total.".format(z,p2)
result_2 = re.search('A total of(.*) Scottish', text)
total_tests = (result_2.group(1))
characters_to_remove = ","
for character in characters_to_remove:
    total_tests = total_tests.replace(character,"")
tot = int(total_tests)
#Calculate proportion of positive tests
pos = round((y/tot)*100,1)
#tot = 'unknown'
#pos = 'unknown'
#The reason for this is HPS have not yet provided today's numTests data. Apologies.
txt_2 = "\n\n Tests: \n The total number of positive cases to date is {}. The total number of tests reported is {}. The percentage of tests which are positive is currently {}%".format(y,tot,pos)

#Are there any new regions?
#new_areas = (df_x[df_x.isnull().any(axis=1)]['Health_Board']).tolist()
#if len(new_areas) == 0:
  #  new_areas = 'None' 
txt_3 = "All health boards have reported cases except: Orkney and The Western Isles"
characters_to_remove = "[]'" 
for character in characters_to_remove:
    txt_3 = txt_3.replace(character,"")
df_x2.columns = df_x2.columns.map(mapper)
#Calculate Increases Per Region - % and absolute
df_x3 = df_x2
df_x3 = df_x3.replace(np.nan, 0, regex=True)
#Increase per row (ignoring total and new areas)
df_x2['Increase'] =  ((df_x2.iloc[:, -1]) - (df_x.iloc[:,-2]))
df_x2['pIncrease'] =  (df_x2['Increase'] / df_x.iloc[:, -2]) * 100
#Increase per row with total and allowing new areas
df_x3['Increase'] =  (df_x2.iloc[:, -3] - df_x2.iloc[:, -4])
df_x3['pIncrease'] =  (df_x2['Increase'] / df_x2.iloc[:, -4]) * 100
#Remove total row so that we can report biggest increase by region, not overall
df_x2b = df_x2.iloc[:-1]
#What is the largest increase in case numbers (in absolute and % terms)
Inc_max = (df_x2b['Increase'].max())
pInc_max = (df_x2b['pIncrease'].max()) 
# Where did these increases occur? 
Inc_maxLoc = list(df_x2b['Health_Board'][df_x2b.Increase == df_x2b.Increase.max()]) 
pInc_maxLoc = list(df_x2['Health_Board'][df_x2.pIncrease == df_x2.pIncrease.max()])       
#Convert to int and return max value
Inc_max = int(Inc_max) 
pInc_max = round(pInc_max,1)
txt_4 = "The largest increase in cases in absolute terms by health board was {} cases, which occurred in {}. The largest increase in cases in relative terms was {}%, which occurred in {}.".format(Inc_max, Inc_maxLoc, pInc_max, pInc_maxLoc)
characters_to_remove = "[]'" 
for character in characters_to_remove:
    txt_4 = txt_4.replace(character,"")
#Set value of Increase and pIncrease
df_x2.loc[['Total'], ['Increase']] = z
df_x2.loc[['Total'], ['pIncrease']] = p2
#Transpose data 
df_xT = df_x2.transpose()



#Doubling time calculations

# ***************************************   Cases ****************************************
#Set date value for lookback
dx_Dt = tDt - timedelta(days=7)
#watch out for date (using yDt for dev, should be tDt in prod)
df_tDt = df_x2
df_tDt.iloc[0,0] = 'Ayrshire'
#Change Datetime Format
df_tDt.columns = df_tDt.columns.map(mapper)
#Drop columns
cols = [0,1,-1,-2]
df_tDt= df_tDt.drop(df_tDt.columns[[cols],], axis=1) 
#watch out for date (using yDt for dev, should be tDt in prod)
df_tDt = df_x2
#Change Datetime Format
cols = [0,-1,-2]
df_tDt_Total = df_tDt.drop(df_tDt.columns[[cols],], axis=1) 
df_tDt_Total = df_tDt_Total.tail(1)
#Set Date Values
t1 = dx_Dt
t2 = tDt
#Calculate index position of dates
z1 = -(abs((tDt - t1).days)+1)
z2 = -(abs((tDt - t2).days)+1)
#Return corresponding cases numbers for dates selected
q1 = df_tDt_Total.iloc[0][z1]
q2 = df_tDt_Total.iloc[0][z2]
tx = abs((t2 - t1).days)
qx = np.log(q2/q1)
ln2 = np.log(2)
Td_C = round(tx * (ln2/qx),2)
txt_Dt_Cases = ('The doubling time for the number of cases over the last week was ~ {} days'.format(Td_C))

# ***************************************   Deaths ****************************************

#Set Date Values
t1 = dx_Dt
t2 = tDt
#Calculate index position of dates
z1 = -(abs((tDt - t1).days)+1)
z2 = -(abs((tDt - t2).days)+1)
#Return corresponding cases numbers for dates selected
q1 = df_Deaths2.iloc[0][z1]
q2 = df_Deaths2.iloc[0][z2]
tx = abs((t2 - t1).days)
qx = np.log(q2/q1)
ln2 = np.log(2)
Td_D = round(tx * (ln2/qx),3)
txt_Dt_Deaths = ('The doubling time for deaths over the last week was ~ {} days'.format(Td_D))

#Calculate Per Capita Metrics

# Bring in population size data
df_pop = pd.read_csv(r"filepath\\Scotland_Population_Size_By_HealthBoard.csv")
#Set string for Ayshire and Arran
df_pop.iloc[0,0] = 'Ayrshire'
#Merge pop data with case data
df_pop2 =  pd.merge(df_tDt, df_pop, how = 'left').sort_values('Health_Board')
df_pop2['Prevalence'] =  (((df_pop2.loc[:,tDt] )/(df_pop2.loc[:,'Pop_Size']))*10000)
df_pop2['Incidence'] =  (((df_pop2.loc[:, 'Increase'])/(df_pop2.loc[:,'Pop_Size']))*10000)
df_pop3 = df_pop2.transpose()
#What is the largest case prevalence and case incidence
prevMax = round(df_pop2['Prevalence'].max(),2)
incdMax = round(df_pop2['Incidence'].max(),2)
# Where did these increases occur? 
prevMaxLoc = list(df_pop2['Health_Board'][df_pop2.Prevalence == df_pop2.Prevalence.max()]) 
incdMaxLoc = list(df_pop2['Health_Board'][df_pop2.Incidence == df_pop2.Incidence.max()]) 
#Exclude Shetland.
df_popx = df_pop2.drop(df_pop2[df_pop2['Health_Board'] == 'Shetland'].index)
prevMax_x = round(df_popx['Prevalence'].max(),2)
prevMaxLoc_x = list(df_popx['Health_Board'][df_popx.Prevalence == df_popx.Prevalence.max()]) 

txt_6 = "The highest incidence over the last day was {}, which occured in {}. \n The highest prevalence is {}, which is in {}. \n The highest prevalence outside of Shetland is {}, which is in {}.".format(incdMax, incdMaxLoc, prevMax, prevMaxLoc, prevMax_x, prevMaxLoc_x)
characters_to_remove = "[]'" 
for character in characters_to_remove:
    txt_6 = txt_6.replace(character,"")

txt_all = "Deaths: \n{} \n{} \n\n Cases: \n{} \n{} \n\n Per Capita (measured per 10,000 head of population) :\n{}\n\n Overall: \n{} \n {}".format(txt_5, txt_Dt_Deaths, txt_1, txt_Dt_Cases, txt_6, txt_4, txt_3, txt_2)

#Export raw case and death data
df_DeathsX.to_excel(r'filepath\\SARS-Cov-2-Scotland-{}_deaths_raw.xlsx'.format(tDt), sheet_name='Deaths') 
df_x2.to_excel(r'filepath\\SARS-Cov-2-Scotland-{}_raw.xlsx'.format(tDt), sheet_name='Total Cases')

#line graph of cases by health board
from pylab import rcParams
#Set figure size
rcParams['figure.figsize'] = 7, 7
#Import Today's Case Data
df_tDt = pd.read_excel (r'filepath\\SARS-Cov-2-Scotland-{}_Raw.xlsx'.format(tDt), sheet_name='Total Cases')
#Drop last two columns and last row
cols = [0,-1,-2]
df_multi = df_tDt.drop(df_tDt.columns[[cols],], axis=1) 
df_multi.drop(df_multi.tail(1).index,inplace=True) 
df_multi.columns = df_multi.columns.map(mapper)
df_multi2 = df_multi.transpose()
#Sort header out
new_header = df_multi2.iloc[0] 
df_multi3 = df_multi2[1:] 
df_multi3.columns = new_header
#Plot
plot = df_multi3.plot.line(title = 'COVID Cases By Health Board', rot=45)
plot.set_ylabel("Num Cases")
plot.legend(loc = 'upper left')
fig2 = plot.get_figure()
fig2.savefig(r'filepath\\cumNumScotland_cases_healthboard_Plot_{}.jpg'.format(tDt),bbox_inches="tight")
#Prevalence
df_prev =  pd.merge(df_multi, df_pop, how = 'left').sort_values('Health_Board')
list_hb = df_prev['Health_Board'].tolist()
df_prev2 = df_prev.iloc[:,1:].div(df_prev['Pop_Size'], axis=0)
df_prev3 = df_prev2 * 10000
df_prev3.insert(0, 'Health_Board', list_hb)
df_prev3 = df_prev3.drop(df_prev3.columns[-1], axis = 1)
df_prev3 = df_prev3.transpose()
#Drop shetland
df_prev3 = df_prev3.drop(df_prev3.columns[-2], axis = 1)
#Sort header out
new_header2 = df_prev3.iloc[0] 
df_prev4 = df_prev3[1:] 
df_prev4.columns = new_header2
#Plot
plot = df_prev4.plot.line(title = 'COVID Prevalence By Health Board', rot=45)
plot.set_ylabel("Cases Per 10k Population")
plot.legend(loc = 'upper left')
fig3 = plot.get_figure()
fig3.savefig(r'filepath\PrevalenceScotland_Plot_{}.jpg'.format(tDt),bbox_inches="tight")

# Create a Pandas Excel writer using XlsxWriter as the engine.
writer = pd.ExcelWriter(r'filepath\SARS-Cov-2-Scotland_all_{}.xlsx'.format(tDt), engine='xlsxwriter')
# Write each dataframe to a different worksheet in same file
df_Deaths_XTran.to_excel(writer, sheet_name='Scotland Deaths')
df_xT.to_excel(writer, sheet_name = 'Scotland Cases')
# Close the writer and output the Excel file.
writer.save()

#Email summary to the team

#Build in some error trapping here

subject = 'COVID-19 in Scotland Daily Report {}'.format(tDt)

img_1 = r'filepath\\cumNumScotland_casesDeaths_Plot_{}.jpg'.format(tDt)
img_2 = r'filepath\\cumNumScotland_cases_healthboard_Plot_{}.jpg'.format(tDt)
img_3 = r'filepath\\PrevalenceScotland_Plot_{}.jpg'.format(tDt)
data = r'filepath\\SARS-Cov-2-Scotland_all_{}.xlsx'.format(tDt)

yag = yagmail.SMTP('emailaddress@gmail.com','superSecurePassword123???')
contents = ['Hello, \n \nHere is the daily report for COVID-19 in Scotland: \n\n' + txt_all +  txt_2 + ' \n \nPlease find the raw data and graphs for cases, deaths and prevalence attached. \n \nPlease note that this is an automated service and replies are not monitored. For any queries, please contact g.calder@ed.ac.uk. \n\n The data in this report are provided by Health Protection Scotland and published daily by the Scottish Government here https://www.gov.scot/coronavirus-covid-19/ \n\n Thank you. ' , img_1 ,img_2, img_3, data]
yag.send(['person1@web.com','person2@web.com'],subject=subject,contents=contents) 


#Send output case and death data to drop box for online doubling calculator app (rShiny)
dbx = dropbox.Dropbox('yourdropboxauthkey123???')

with open(r'C:\Users\gcalder2\Daily_Reports\SARS-Cov-2-Scotland-{}_raw.xlsx'.format(tDt), "rb") as f:
    dbx.files_upload(f.read(), '/Daily_scraper_reports/SARS-Cov-2-Scotland-{}_raw.xlsx'.format(tDt), mute = True)

with open(r'C:\Users\gcalder2\Daily_Reports\SARS-Cov-2-Scotland-{}_deaths_raw.xlsx'.format(tDt), "rb") as f:
    dbx.files_upload(f.read(), '/Daily_scraper_reports/SARS-Cov-2-Scotland-{}_deaths_raw.xlsx'.format(tDt), mute = True)
    
#Return meta data
#print(dbx.files_get_metadata('/SARS-Cov-2-Scotland-2020_03_22_raw.xlsx'.format(yDt)).server_modified)




#Prevalence
df_prev =  pd.merge(df_multi, df_pop, how = 'left').sort_values('Health_Board')
list_hb = df_prev['Health_Board'].tolist()
df_prev2 = df_prev.iloc[:,1:].div(df_prev['Pop_Size'], axis=0)
df_prev3 = df_prev2 * 10000
df_prev3.insert(0, 'Health_Board', list_hb)
df_prev3 = df_prev3.drop(df_prev3.columns[-1], axis = 1)
df_prev3 = df_prev3.transpose()
#Drop shetland
df_prev3 = df_prev3.drop(df_prev3.columns[-2], axis = 1)
#Sort header out
new_header2 = df_prev3.iloc[0] 
df_prev4 = df_prev3[1:] 
df_prev4.columns = new_header2
#Plot
plot = df_prev4.plot.line(title = 'COVID Prevalence By Health Board', rot=45)
plot.set_ylabel("Cases Per 10k Population")
plot.legend(loc = 'upper left')
fig3 = plot.get_figure()
fig3.savefig(r'C:\Users\gcalder2\Daily_Reports\PrevalenceScotland_Plot_{}.jpg'.format(tDt),bbox_inches="tight")



